1. Extract Variable (Erottele muuttuja)

Menetelm�ss� erotellaan osa jotain monimutkaismpaa ilmaisua muuttujaan, johon lasketaan ilmaisun arvo.
Aikaisemman ilmaisun poistettu osa korvataan t�ll� uudella muuttujalla. Muuttujia voi olla my�s useita
ja niille annetut nimet helpottavat koodin loogisen rakenteen hahmottamista.

K�yt�n itse paljon, varsinkin silloin kun ehtolauseen ehdossa ja lauseen sis�ll� k�ytet��n
samaa pitk�hk�� ilmaisua.


2. Remove Middle Man ()

Koodissa on yksinkertainen luokka, jonka metodit (tai osa niist�) yksinkertaisesti siirt�v�t (delegoivat)
metodikutsun toiselle luokalle. T�m� voidaan v�ltt�� niin, ett� metodikutsu tehd��n suoraa luokalle, jota
tarvitaan ilman, ett� v�liss� on toiminnollisesti ontto luokka.


3. Collapse Hiararchy

Yl�luokalla on vaan yksi aliluokka, jonka toiminnallisuus on hyvin samankaltainen kuin yl�luokan toiminnallisuus.
Usein t�llaiset luokat voidaan yhdist�� yhdeksi luokaksi ilman periytyvyytt�. Kannattaa kuitekin huomioida, ett�
koodin laajennettavuus saattaa k�rsi� t�m�n seurauksena, jos on mahdollista, ett� aliluokkia tullaan joskus tarvit-
semaan lis��. T�ll�in uusien aliluokkien koheesio on huonompi, koska ne voivat sis�lt�� ylim��r�isi� toiminnallisuuksia.


4. Consolidate Duplicate Conditional Fragments (Yhdist� moninkertaiset ehtolauseen osat)

Jos koodissa on ehtolause, jonka jokaisessa lohkossa (if ja else) tehd��n muun ohella yksi sama asia, voidaan yhteiset
suoritettavat koodirivit tuoda silmukan ulkopuolelle. T�m� ei v�ltt�m�tt� toimi silloin, kun yhteinen koodirivi suoritetaan
muun koodin v�liss�, tai silloin kun ehtolause on try-catch -lohkon sis�ll�.


5. Preserve Whole Object (S�ilyt� olio kokonaisena)

Koodissa kutsutaan erin�isi� funktioita tai metodeja, joiden parametriksi l�hetet��n useita olion attribuutteja. 
Koodi on todenn�k�isemmin siistimpi silloin, kun parametriksi l�hetet��n koko olio, ja kutsuttava metodi huolehtii siit�,
mit� attribuutteja se tarvitsee. T�m� v�het�� tarvittavien parametrien m��r��, mutta usein lis�� koodissa olevien kytkent�jen
lukum��r��.