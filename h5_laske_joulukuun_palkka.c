

#define ERROR -1

double laske_joulukuun_palkka ( 
	double kk_palkka, 	// henkil�n kuukausipalkka euroissa: 1500.00 - 3500.00
	int palvelusaika	// henkil�n palvelusaika yrityksess� vuosina: 0 - 50
)
{
	double bonusaste = 1.0;

	if (palvelusaika < 0 || palvelusaika > 50) {
		return ERROR;
	
	} else if (kuukausipalkka < 1500 || kuukausipalkka > 3500) {
		return ERROR;
	
	} else if (palvelusaika >= 9){
		bonusaste = bonusaste * 2.0;
		
	} else if (palvelusaika >= 6){
		bonusaste = bonusaste * 1.75;
		
	} else if (palvelusaika >= 3){
		bonusaste = bonusaste * 1.5;
		
	}
	
	return kk_palkka * bonusaste;
}

