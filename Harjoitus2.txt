Skenaarioita

1.
Ohjelmistotalon työntekijä luo tunnuksen keskustelujärjestelmään.
Ylläpitäjä lisää tunnuksen järjestelmään.
Työntekijä käynnistää graafisen käyttöliittymän ja kirjautuu uudella tunnuksellaan.
Järjestelmä ilmoittaa uuden käyttäjän kirjautumisesta.
Järjestelmä esittää käytäjälle toimintavaihtoehdot.
Työntekijä liittyy kehittäjätiimin keskustelukanavalle.
Järjestelmä näyttää käyttäjälle kanavalle kirjoitetut viestit.
Työntekijä aloittaa yksityisen keskustelun kollegansa kanssa.
Järjestelmä luo uuden yksityisen kanavan ja lisää molemmat käyttäjät sille.
Työntekijä kirjoittaa tervehdyksen.
Kollega lukee viestin ja vastaa tervehdykseen.
Työntekijä kirjautuu ulos järjestelmästä.
Järjestelmä ilmoittaa uloskirjautumisesta.

2.
Henkilöstöpäällikkö kirjautuu sisälle järjsetelmään.
Järjestelmä esittää hänelle toimintavaihtoehdot.
Henkilöstöpäällikkö luo uuden keskustelukanavan nimeltä ohjelmistosuunnittelu.
Henkilöstöpäällikkö antaa suunnitteluvastaavalle väliaikaset oikeudet kanavalle.
Henkilöstöpäällikkö kirjautuu ulos järjestelmästä.

3.
Ylläpitäjä lähettää tiedotteen koskien järjestelmän huoltokatkosta.
Järjestelmä välittää tiedotteen jokaiselle keskustelukanavalle.
Sovitun ajan kuluttua ylläpitäjä sulkee palvelimen.
Huollon jälkeen ylläpitäjä käynnistää palvelimen uudelleen.

Käyttötapauskaavio tiedostossa usecase01.html
luotu käyttäen draw.io -palvelua.

