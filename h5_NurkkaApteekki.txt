Järjestelmä täytyy testata sekä asiakkaan (ikäihmisen) että lääkärin käyttöliittymän kautta.

Yhteensopivuus:
Verkkosivu testataan eri käyttäjärjestelmillä ja selaimilla, sekä useiammilla eri mobiililaitteilla.
Kirjautumista tulisi testata useiden eri nettipankkien kautta.

Toimivuus:
Tarkistetaan, että yhteys tietokantaan toimii oikein.
Testataan erinäisiä lääkereseptiyhdistelmiä. Testiin otetataan mukaan yhdistelmiä, jotka eivät sisällä vaarallisia
yhteisaikutuksia sekä sellaisia yhdistelmiä, jotka muodostavat vaarallisia sivuvaikutuksia. 

Käytettävyys:
Testataan verkkosivuja käytännössä eri käyttäjäryhmien edustajen kanssa. Otetaan huomioon esillenousevia ongelmakohtia. 

Saavutettavuus:
Koska sivu on suunniteltu ikäihmisten käyttöön, korostuvat sivun helppokäyttöisyysomineisuudet.
Esimerkiksi tekstin on oltava riittävän selkeää, jotta huononäköisetkin voivat saada siitä selvää ongelmitta.
Testataan helppokäyttöisyysomineisuuksia käyttäjäryhmän edustajilla.

Suorituskyky:
Testataan sivuston vasteaikoja eri ympäristöissä.

Kuormitustestaus:
Testataan verkkosivun jatkuvaa käyttöä esimerkiksi sadan yhtäaikaisen käyttäjän tapauksessa. Etsitään mahdollisia
pullonkauloja sivuston suorituskyvyssä.

Piikkitestaus:
Testataan miten verkkosivu käyttäytyy silloin, kun sivua ylikuormitetaan. Testataan myös, että sivu palautuu
kuormituksesta oikein, eikä virheitä synny kuormituksen aikana esimerkiksi tietokantaan.

Turvallisuus:
Turvallisuus on tärkeää, koska sivulla käsiteltään yksityistä ja luottamuksellista tietoa.
Testataan, ettei käyttäjät pääse muille käyttäjille kuuluvaan tietoon. Testataan testiryhmän avulla, ettei
tietoa voi hävittää vahingossa. Testataan, ettei verkkosivulla ole yleisiä tietoturva-aukkoja.


